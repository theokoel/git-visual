import * as path from "path";
import * as vscode from "vscode";
import { GenSVG } from "git-graph-for-node";
import * as child_process from "child_process";

export function activate(context: vscode.ExtensionContext) {
  context.subscriptions.push(
    vscode.commands.registerCommand("gitVisual.showGit", () => {
      GitVisualPanel.createOrShow(context.extensionPath);
    })
  );

  // context.subscriptions.push(
  //   vscode.commands.registerCommand("catCoding.doRefactor", () => {
  //     if (GitVisualPanel.currentPanel) {
  //       GitVisualPanel.currentPanel.doRefactor();
  //     }
  //   })
  // );

  if (vscode.window.registerWebviewPanelSerializer) {
    // Make sure we register a serializer in activation event
    vscode.window.registerWebviewPanelSerializer(GitVisualPanel.viewType, {
      async deserializeWebviewPanel(
        webviewPanel: vscode.WebviewPanel,
        state: any
      ) {
        console.log(`Got state: ${state}`);
        GitVisualPanel.revive(webviewPanel, context.extensionPath);
      }
    });
  }
}

/**
 * Manages git visual webview panels
 */
class GitVisualPanel {
  /**
   * Track the currently panel. Only allow a single panel to exist at a time.
   */
  public static currentPanel: GitVisualPanel | undefined;

  public static readonly viewType = "gitvisual";

  private readonly _panel: vscode.WebviewPanel;
  private readonly _extensionPath: string;
  private _disposables: vscode.Disposable[] = [];

  public static createOrShow(extensionPath: string) {
    const column = vscode.window.activeTextEditor
      ? vscode.window.activeTextEditor.viewColumn
      : undefined;

    // If we already have a panel, show it.
    if (GitVisualPanel.currentPanel) {
      GitVisualPanel.currentPanel._panel.reveal(column);
      return;
    }

    // Otherwise, create a new panel.
    const panel = vscode.window.createWebviewPanel(
      GitVisualPanel.viewType,
      "Git Visual",
      column || vscode.ViewColumn.One,
      {
        // Enable javascript in the webview
        enableScripts: true,

        // And restrict the webview to only loading content from our extension's `media` directory.
        localResourceRoots: [vscode.Uri.file(path.join(extensionPath, "media"))]
      }
    );

    GitVisualPanel.currentPanel = new GitVisualPanel(panel, extensionPath);
  }

  public static revive(panel: vscode.WebviewPanel, extensionPath: string) {
    GitVisualPanel.currentPanel = new GitVisualPanel(panel, extensionPath);
  }

  private constructor(panel: vscode.WebviewPanel, extensionPath: string) {
    this._panel = panel;
    this._extensionPath = extensionPath;

    // Set the webview's initial html content
    this._update();

    // Listen for when the panel is disposed
    // This happens when the user closes the panel or when the panel is closed programatically
    this._panel.onDidDispose(() => this.dispose(), null, this._disposables);

    // Update the content based on view changes
    this._panel.onDidChangeViewState(
      e => {
        if (this._panel.visible) {
          this._update();
        }
      },
      null,
      this._disposables
    );

    // Handle messages from the webview
    this._panel.webview.onDidReceiveMessage(
      message => {
        switch (message.command) {
          case "alert":
            vscode.window.showErrorMessage(message.text);
            return;
        }
      },
      null,
      this._disposables
    );
  }

  public doRefactor() {
    // Send a message to the webview webview.
    // You can send any JSON serializable data.

    child_process.exec(
      'git log --all --date-order --pretty="%H*%P*%d|"',
      { cwd: this._extensionPath },
      (err, stdout, stderr) => {
        if (err || stderr) {
          console.log("err || stderr");
          console.log(err);
          console.log(stderr);
        }
        if (stdout) {
          this._panel.webview.postMessage({
            command: "gitGraph",
            gitLog: stdout,
            gitGraph: this.getGitSvg(stdout)
          });
        }
      }
    );

    this._panel.webview.postMessage({ command: "refactor" });
  }

  public dispose() {
    GitVisualPanel.currentPanel = undefined;

    // Clean up our resources
    this._panel.dispose();

    while (this._disposables.length) {
      const x = this._disposables.pop();
      if (x) {
        x.dispose();
      }
    }
  }

  private _update() {
    const webview = this._panel.webview;
    this._panel.title = "Git Visual";
    this._panel.webview.html = this._getHtmlForWebview(webview);
    this.doRefactor();
  }

  private _getHtmlForWebview(webview: vscode.Webview) {
    const scriptMainPathOnDisk = vscode.Uri.file(
      path.join(this._extensionPath, "media", "main.js")
    );
    const scriptMainUri = webview.asWebviewUri(scriptMainPathOnDisk);

    const scriptGPathOnDisk = vscode.Uri.file(
      path.join(this._extensionPath, "media", "g.js")
    );
    const scriptGUri = webview.asWebviewUri(scriptGPathOnDisk);

    // Use a nonce to whitelist which scripts can be run
    const nonce = getNonce();

    return `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src ${webview.cspSource} https:; script-src 'nonce-${nonce}';">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <script src="https://cdn.jsdelivr.net/npm/@gitgraph/js"></script> 
                <title>Git Visual</title>
            </head>
            <body>
                <h1 id="lines-of-code-counter">0</h1>
                <p id="textTempo">Salut</p>
                <div id="gitGraphContent"></div>
                <script nonce="${nonce}" src="${scriptMainUri}"></script>
                <script nonce="${nonce}" src="${scriptGUri}"></script>
            </body>
            </html>`;
  }

  private getGitSvg(gitLog: string) {
    console.log(gitLog);
    const gitGraph = new GenSVG();
    gitGraph.setting({ radius: 5, wSpacing: 25, hSpacing: 25 });
    const svg = gitGraph.generate(gitLog);
    return svg;
  }
}

function getNonce() {
  let text = "";
  const possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (let i = 0; i < 32; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}
